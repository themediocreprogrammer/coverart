# The Mediocre Programmer Cover Art

This is the repository for the original files for The Mediocre Programmer.

## Artworks/License:

Cover art is CC-By 4.0 International to David Revoy.
